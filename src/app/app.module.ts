import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule,Routes}from '@angular/router';
import {SocketIoConfig,SocketIoModule} from 'ngx-socket-io';
const config: SocketIoConfig = {url: 'http://localhost:4444',options:{}};
import { AppComponent } from './app.component';
import { CreatedeviceComponent} from './createdevice/createdevice.component';
import { ReadDeviceComponent } from './read-device/read-device.component'
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { EditdeviceComponent } from './editdevice/editdevice.component';
import { MatchtagsComponent } from './matchtags/matchtags.component';
import { TrakingComponent } from './traking/traking.component';
const routes: Routes = [
  {path:'devicecreation', pathMatch:'full',component:CreatedeviceComponent},
  {path:'readdevice', pathMatch: 'full', component: ReadDeviceComponent},
  {path: 'devices', pathMatch:'full', component: EditdeviceComponent },
  {path: 'devices/:tid', pathMatch:'full', component: TrakingComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CreatedeviceComponent,
    ReadDeviceComponent,
    EditdeviceComponent,
    MatchtagsComponent,
    TrakingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  exports:[CreatedeviceComponent,ReadDeviceComponent, EditdeviceComponent, MatchtagsComponent],
  bootstrap: [AppComponent]
}) 
export class AppModule { }
