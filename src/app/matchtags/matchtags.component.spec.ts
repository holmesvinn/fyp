import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchtagsComponent } from './matchtags.component';

describe('MatchtagsComponent', () => {
  let component: MatchtagsComponent;
  let fixture: ComponentFixture<MatchtagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchtagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchtagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
