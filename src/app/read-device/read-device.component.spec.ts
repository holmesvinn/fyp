import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadDeviceComponent } from './read-device.component';

describe('ReadDeviceComponent', () => {
  let component: ReadDeviceComponent;
  let fixture: ComponentFixture<ReadDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
