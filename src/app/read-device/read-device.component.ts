import { Component, OnInit } from '@angular/core';
import { SocketserviceService } from '../socketservice.service';
import { Subscription } from 'rxjs';
import { DeviceServiceService } from '../device-service.service';

@Component({
  selector: 'app-read-device',
  templateUrl: './read-device.component.html',
  styleUrls: ['./read-device.component.css']
})
export class ReadDeviceComponent implements OnInit {

  private mlogs: Subscription;
  private isRead: boolean = false;
  private readData;
  private dmsg:string = "please place your tag in reader";
 
  
  constructor(private socservice: SocketserviceService, private dbdata:DeviceServiceService ) { }

  ngOnInit() {
    
    this.mlogs = this.socservice.mlog.subscribe(data => {
      this.retrieveId(data)
    });
    this.sendMessage('read');
  }
  retrieveId(deviceID) {

    this.dbdata.readDevice(deviceID).subscribe(data => {
      console.log("reading invalid",data);
      if(data == null || data == undefined){
        this.dmsg = "Invalid Card has been detected";
      }
    else{  
      this.readData = data; 
        this.isRead = true;
    }
    },(error) => {
      this.dmsg = "Invalid Card detected";
      this.isRead = true;
    }); 
  }

  sendMessage(text){
    this.socservice.sendMessage(text);
  }

}
