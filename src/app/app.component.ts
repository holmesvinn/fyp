import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
 
  constructor(private router: Router){ }
  

  createDevice(){
    console.log("createDevice");
    this.router.navigate(['/devicecreation']);
  }

  TrackDevice(){
    console.log("tracking Device");
    this.router.navigate(['/readdevice']);
  }

  EditDevice(){
    console.log("editdevice");
    this.router.navigate(['/devices'])
  }

  ngOnInit(){
    this.router.navigate(['/devicecreation']);
  }


}
