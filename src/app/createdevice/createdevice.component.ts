import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DeviceServiceService } from '../device-service.service';
import { Device } from '../device';
import { Subscription } from 'rxjs';
import { SocketserviceService } from '../socketservice.service';


@Component({
  selector: 'app-createdevice',
  templateUrl: './createdevice.component.html',
  styleUrls: ['./createdevice.component.css']
})
export class CreatedeviceComponent implements OnInit {

  private mlogs: Subscription;
  private devicedata : Device;
  private formshow:boolean = true;
  private loadershow:boolean = false;
  private messageshow:boolean = false;

  private Message:String = '';
  private date;
  constructor(private data:DeviceServiceService, private http: HttpClient, private socservice: SocketserviceService) { }

  ngOnInit() {

    this.mlogs = this.socservice.mlog.subscribe(data => {
      if(data == 'success'){
        this.Message =  " successfully wrote rfid tag";
        this.messageshow = true;
        this.loadershow = false;
        this.formshow = false;
      }
    });
  }

  createRFID(device){
    this.formshow = false; 
    this.loadershow = true;
    this.date = Date();
    this.devicedata = {
      location: device.value.location,
      device_name: device.value.dn,
      from_org: device.value.on,
      device_cost:device.value.dc,
      tracking_hardware: device.value.trackingha,
      device_spec: device.value.specifications,
      device_usage: device.value.deviceUsage
    }
    console.log(this.devicedata); 

    this.data.createDevice(this.devicedata).subscribe((res)=>{
      console.log(res);

      this.loadershow = true; 
      this.messageshow = true;
      this.Message = "Successfully Posted Data in Database and retrived ID please place your card to write data";
      this.socservice.sendMessage('write');
      this.socservice.sendMessage(res._id);
    },(error)=>{
      console.log(error);
      this.Message = "Cannot Post data in Database Some Error has occured";
      this.messageshow = true; 
      this.loadershow = false;
      this.formshow = false;
    });
  }
}
