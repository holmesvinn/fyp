import { Injectable } from '@angular/core';
import {Socket} from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})

export class SocketserviceService {

  mlog = this.socket.fromEvent<string>('logs');
  constructor(private socket: Socket) { }
  
  sendMessage(msg:string){
    this.socket.emit('message', msg);
  }
}
