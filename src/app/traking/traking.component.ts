import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SocketserviceService } from '../socketservice.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-traking',
  templateUrl: './traking.component.html',
  styleUrls: ['./traking.component.css']
})
export class TrakingComponent implements OnInit {

  private  rssi_value = ""; 
  private tid = "";
  private device_msg = "";
  private counter = 0;
  private rssivalues: Subscription;
  private prev_data = "vikky";
  private my_dev = false;
  private scan: boolean = true;

  constructor(private route: ActivatedRoute, private socservice: SocketserviceService) { }

  ngOnInit() {
    this.tid = this.route.snapshot.paramMap.get('tid');
    this.rssivalues = this.socservice.mlog.subscribe(data => {
        this.trackDevice(data);
    });
    this.socservice.sendMessage("track");
    this.socservice.sendMessage(this.tid);
  }
  
  trackDevice(data) {

    console.log(data);
      this.counter = this.counter + 1;
      if(this.counter > 7)
      {
        this.counter = 1;
        this.socservice.sendMessage("scan");
      }
      else{
        this.socservice.sendMessage("ok");
      }


    if(data.indexOf("not") >  -1){
        this.rssi_value = "Not Available";
        this.device_msg = "Device Absent";
    }
    else {
    if(data.indexOf("RSSI") > -1){
      this.rssi_value = data;
      this.device_msg = "Device is Present in Current Lab";
    }
    else{

      if(this.counter == 1){
        this.rssi_value = "Not Available"
        this.device_msg = "Device doesnt seem to be available";
        this.my_dev = false;
      }
      else if(this.rssi_value != "Not Available"){
        this.device_msg = "device appears to be static position with previous rssi"        
      }
         
    }
  }
  }

}
