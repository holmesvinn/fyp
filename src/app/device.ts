export interface Device  {
    device_name: String,
    device_cost:String,
    location:String,
    from_org:String,
    tracking_hardware: String,
    device_spec: String,
    device_usage: String
}
