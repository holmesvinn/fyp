import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Device } from './device';
import {catchError} from 'rxjs/operators'; 
import { environment } from '../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
 
@Injectable({
  providedIn: 'root'
})
export class DeviceServiceService {

  constructor(private http: HttpClient) { }

  createDevice(devicedata: any){

   return this.http.post<any>(environment.postendpoint, devicedata, httpOptions);

  }

  readAlldevices(){
    return this.http.get(environment.getendpoint+ "/all")
  }
 
  readDevice(deviceId){

    return this.http.get(environment.getendpoint+deviceId);

  }
}
