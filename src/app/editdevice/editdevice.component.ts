import { Component, OnInit } from '@angular/core';
import { DeviceServiceService } from '../device-service.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-editdevice',
  templateUrl: './editdevice.component.html',
  styleUrls: ['./editdevice.component.css']
})
export class EditdeviceComponent implements OnInit {

  devices = [];

  constructor(private dbdata: DeviceServiceService, private router: Router ) { }

  ngOnInit() {
    this.dbdata.readAlldevices().subscribe(data => {
      for (var key in data) {
        this.devices.push(data[key])
     }
    });
  }


  BleTracking(tid){
    this.router.navigate(['/devices',tid])
  }

}
